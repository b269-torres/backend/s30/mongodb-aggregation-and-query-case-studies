db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);

//Count of total number of fruits on sale
db.fruits.aggregate([
    {$match: {onSale: true}},
    {
      $count: "fruitsOnSale"
    }
  ]);



//Count the total number of tfruits with stock more than 20

db.fruits.aggregate([
    {$match: {stock: {$gte: 20}}},
    {
      $count: "enoughStock"
    }
  ]
);

 //Average price of fruits onSale per supplier

 db.fruits.aggregate([
   {$match: {onSale: true}},
     {$group:{_id: "$supplier_id", avg_price: { $avg: "$price" } }},
     { $sort : { _id : 1} }
   ]);
 //Get the highest price of a fruit per supplier

 db.fruits.aggregate([
   {$match: {onSale: true}},
    {$group:{_id: "$supplier_id", highest_price: { $max: "$price" } }},
  { $sort : { _id : -1} }
  ]);


//Get the lowest price of a fruit per supplier
 db.fruits.aggregate([
   {$match: {onSale: true}},
    {$group:{_id: "$supplier_id", min_price: { $min: "$price" } }},
   { $sort : { _id : -1} }
  ]);